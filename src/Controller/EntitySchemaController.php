<?php

namespace Drupal\schema_ui\Controller;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

class EntitySchemaController {

  public function title() {
    return t('Schema');
  }

  public function content() {
    $request = \Drupal::request();

    $entity_type_id = $request->get('entity_type_id');


    /** @var \Drupal\node\Entity\NodeType $entity_type */
    $entity_type = $request->get($entity_type_id . '_type');
    $bundle = $entity_type->getOriginalId();

    // list all fields of entity type
    $fields = \Drupal::service('entity_field.manager')->getFieldDefinitions($entity_type_id, $bundle);

    $rows = [];
    foreach ($fields as $field_name => $field) {


      $columns = [];

      if ($field instanceof FieldStorageDefinitionInterface) {
        $columns = $field->getColumns();
        $cartinality = $field->getCardinality();
      }
      if ($field instanceof \Drupal\field\Entity\FieldConfig) {
        $columns = $field->getFieldStorageDefinition()->getColumns();
        $cartinality = $field->getFieldStorageDefinition()->getCardinality();
      }

      if ($cartinality === BaseFieldDefinition::CARDINALITY_UNLIMITED) {
        $cartinality = 'Unlimited';
      }

      if (is_int($cartinality)) {
        $cartinality = 'Limited ('.$cartinality.')';
      }

      $rows[] = [
        [
          'data' => [
            '#type' => 'html_tag',
            '#tag' => 'code',
            '#value' => $field_name,
          ]
        ],
        $field->getType(),
        $cartinality,
        $field->getLabel()
      ];

      foreach ($columns as $key => $item) {
        $rows[] = [
          [
            'data' => [
              '#markup' => sprintf('└─ <code>%s</code> <small>(%s)</small>', $key, $item['type']),
            ]
          ],
          '',
          '',
          $item['description'] ?? ''
        ];
      }

    }

    $header = [
      'id' => t('Key'),
      'type' => t('Type'),
      'cardinality' => t('Cardinality'),
      'title' => t('Title'),
    ];
    $build['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => t('No content has been found.'),
      '#attached' => [
        'library' => [
          'schema_ui/table_styling',
        ],
      ],
      '#attributes' => [
        'class' => [
          'schema-ui-table',
        ],
      ],
    ];
    return $build;

  }

}
